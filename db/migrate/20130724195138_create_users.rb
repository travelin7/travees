class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username, :limit => 100
      t.string :fullname, :limit => 100
      t.string :email, :limit => 100
      t.string :password, :limit => 255

      t.timestamps
    end
  end
end
